# Changes made 
## 1.[Setup Database](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/ROPS%20database%20restore.png)
**1.1 Setup Database in Microsoft SQL server Management Studio:**  
1.  Get the ROPS database dump file
2.  Right Click Database> Device> Three dot> Add> Select all files> Choose the dump file for ROPS> ok
3.  Database setup done

**1.2 Add [login name](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/Create%20login%20ROPS.png) and [adding role rops to sysadmin](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/Multiuser.png)**  and change the password for [`ROPSMAKERADMIN@ROPS`](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/changing%20password.png) and [`ROPSCHECKERADMIN@ROPS`](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/changing%20password.png).

	GO
	CREATE LOGIN ROPS
	 WITH PASSWORD = 'ROPS',
	CHECK_POLICY = OFF,
	CHECK_EXPIRATION = OFF,
	DEFAULT_DATABASE = ROPS;
	GO
	EXEC sp_addsrvrolemember
	 @loginame = N'ROPS',
	 @rolename = N'sysadmin';
	GO
	use ROPS
	CREATE USER ROPS FROM LOGIN ROPS
	EXEC sp_addrolemember
	N'db_owner', 'ROPS';
	GO
	ALTER DATABASE ROPS SET MULTI_USER;
	GO
	
	update jfw_users set password='17cc5195319e5a7e2b1347e49bc9ce5c' where username = 'ROPSMAKERADMIN@ROPS';
	GO
	
	update jfw_users set password='0fc11d64a8553b283948c3224f1ef010' where username = 'ROPSCHECKERADMIN@ROPS';
	GO
>NOte: The password will be changed to "123" for ropsmaker and ropschecker. After that we have to change it while logging in. [**ROPSMAKERADMIN@ROPS** **ROPSCHECKERADMIN@ROPS**](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/corepay%20epg%20ropsmaker%20pass.png)

**1.3 [set the Environment Variables of database](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/Environment%20variable.png).**  
Go to cmd and execute the following parameters for database environment or either you can setup by gui mode.  
```
setx /m db_url1 "jdbc:jtds:sqlserver://localhost:1433/ROPS"
setx /m db_user1 "ROPS"
setx /m db_password1 "ROPS"
setx /m db_schema1 "ROPS"
```
**1.4 Verify if the environment has properly set or not.**  
Go to command prompt and execute `echo %variable_name%` it should return the value

#### 1.5 Extracting the corpay war file in the webapps directory of corpay tomcat folder.
[**Image**](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/Extracting%20war%20file.png)


## 2.Setup Tomcat for CORPAY

**Tomcat installation and configuration for CORPAY**  

Following are the files where changes have to be made.  

1. [server.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/AJP%20port.png) in the location c:\corpay\corpaytomcat\tomcat 8.5\conf
2. [applicationContext-datasources.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/application%20context%20datasources%20check.png) in the location C:\corpay\corpaytomcat\tomcat 8.5\webapps\ROPS\WEB-INF\classes\config\core\spring
3. [datasource.properties](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/datasource%20properties%20check.png) in the location C:\corpay\corpaytomcat\tomcat 8.5\webapps\ROPS\WEB-INF\properties

**2.1. server.xml**
Change the shutdown port to `9009`, change the connector port number to `9999` for the `HTTP` protocol, change the connector port to `9015`, and redirecting port to `9444` for the `AJP` protocol.

**2.2. applicationContext-datasources.xml**
Check if the properties are well set with the environment variables that we set. 
```
<bean id="jfwDataSource" class="org.apache.commons.dbcp.BasicDataSource"> 
 <property name="driverClassName" value="${db_driver}" /> 
 <property name="url" value="${db_url1}" /> 
 <property name="username" value="${db_user1}" /> 
 <property name="password" value="${db_password1}" /> 
 <property name="validationQuery" value="${db_validation_query}" /> 
 <property name="maxWait" value="10000" /> <property name="maxIdle" value="30" /> 
 <property name="testOnBorrow" value="true"></property> 
 <property name="testOnReturn" value="true"></property> 
 <property name="maxActive" value="100" /> 
</bean>
```
**2.3. datasource.properties**
Check if the database properties match with the environment variables or not.
```
#oracle : jdbc:oracle:thin:@ip:1521:PSDB
#MsSql : jdbc:jtds:sqlserver://ip:1433/dbname
#LOG4JDBC : add log4jdbc: after jdbc:
connection.url=db_url1
datasource.user=db_user1
datasource.password=db_password1
datasource.schema=db_schema1
```
## 3. Activemq Configuration  

**Configure Activemq**  

1. Navigate to conf folder in the ActiveMQ's installation location. 
2. Inside the conf sub-directory we have to make some changes in two files (activemq.xml, jetty.xml).

**[activemq.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/active%20MQ%20transport%20connectors.png)**:
Comment out the transportConnectors except the operwire and also change the port for openwire to `61617`, and change the maximumConnections to 10000 from 1000, and change the brokerName to "CORPAY_AMQ".
```
<transportConnectors>
<!-- DOS protection, limit concurrent connections to 1000 and frame size to 100MB -->
<transportConnector name="openwire" uri="tcp://0.0.0.0:61617?maximumConnections=10000&amp;wireFormat.maxFrameSize=104857600"/>
<!--<transportConnector name="amqp" uri="amqp://0.0.0.0:5672?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="stomp" uri="stomp://0.0.0.0:61613?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="mqtt" uri="mqtt://0.0.0.0:1883?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="ws" uri="ws://0.0.0.0:61614?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/> -->
</transportConnectors> 
```
**[jetty.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/jetty%20port%20changed%20to%208162.png)**:
Change the port number to `8162`. The default port number is `8161`. 

## 4. Starting the CorPay Application and Activemq
> **Note: `Activemq` should be started first and then `tomcat`.**

**4.1 Starting Activemq**  
Navigate to C:/corpay/corepay activemq/bin/win64 and run `activemq` windows batch file  

**4.2 Starting tomcat**  
Navigate to C:/corpay/corepay epg/tomcat8.5/bin/ and run startup windows batch file  

**4.3 Login URL for Activemq:**  
`localhost:8162/`

**4.4 Login URL for CorPay:**  
`localhost:9999/ROPS/`

**4.5 Default login Credentials for activemq admin**  
```
Username: admin  
password: admin
```
## 5. Integration of CORPAY with EPG
  
**5.1 Following are the files that need to be configured.**  

1. push-action-camel-config.xml
2. applicationContext-integration.xml
3. app-settings.properties

> NOte: Above file can be found inside the following locations respectively:
> 1. C:\corpay\corpay tomcat\tomcat 8.5\webapps\ROPS\WEB-INF\classes\config\core\camel\push-action-camel-config.xml
> 2. C:\corpay\corpay tomcat\tomcat 8.5\webapps\ROPS\WEB-INF\classes\config\core\spring\applicationContext-integration.xml
> 3. C:\corpay\corapy tomcat\tomcat 8.5\webapps\ROPS\WEB-INF\classes\config\app\spring\app-settings.properties
---
**Change the IP address to localhost in the following files:**

**5.1.1**  `push-action-camel-config.xml`
```
<bean id="activemq" class="org.apache.activemq.ActiveMQConnectionFactory"> 
	<property name="brokerURL" value="${env_broker_url:tcp://localhost:61617}" />
</bean> 
```
**5.1.2** `applicationContext-integration.xml`
```
<bean id="jfwActiveMqFactory" class="org.apache.activemq.ActiveMQConnectionFactory"> 
	<property name="brokerURL" value="${env_broker_url:tcp://localhost:61617}" /> 
	<property name="redeliveryPolicy" ref="redeliveryPolicy" />
</bean>
```

**5.1.3**  `app-settings.properties`
```
#################################################################################
corporate.account.balance.activemq.url=tcp://localhost:61617
corporate.account.balance.activemq.username=HBLIPS1
corporate.account.balance.activemq.password=hBl@2018
corporate.account.balance.request.queue.name=account_balance_request
corporate.account.balance.response.queue.name=account_balance_response
corporate.account.balance.time.to.live=600000
update.account.balance.timer=3600000
################################################################################
payment.allowance.time.to.live=1
################################################################################
ach.activemq.url=tcp://localhost:61616
ach.activemq.username=admin
ach.activemq.password=admin
ach.inward.queue.name=rops.mt.outward
ach.outward.queue.name=rops.mt.inward
ach.mt.generation.timer=60000
################################################################################
onus.activemq.url=tcp://localhost:61616
onus.activemq.username=admin
onus.activemq.password=admin
onus.inward.queue.name=rops.csv.onus.outward
onus.outward.queue.name=rops.csv.onus.inward
onus.csv.generation.timer=60000
################################################################################
```
> NOte: Now we can use CORPAY application but before starting the CORPAY application EPG must be running.

---
#### ROPSMAKERADMIN HOMEPAGE
![ROPSMAKER](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/maker.png)

#### ROPSCHECKERADMIN HOMEPAGE
![ROPSCHECKER](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20Screenshots/checker.png)
