## Full payment cycle on CORPAY

**Steps:**

1.  Login to CORPAY application as `maker@mk` user.

> _maker@mk_: user that has the maker role.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/corpay%20login.png)

2.  Navigate to `Payment>Payment Template` and click `new item` to initiate a new payment.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/step1.png)
    
3.  Enter the details of the payment and click the `create` button to create the payment.
    ![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/creating%20payment.png)

> New payment template will be created.

4.  Scroll down and click the `New Transaction` button to create a new transaction item.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/creating%20new%20transation.png)

5. Choose the beneficiary account. 
 ![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/beneficiary%20account.png)

6. Enter the transaction details of the new transaction item and click the `create` button.

7.  After the creation of the transaction item fully completes, click the `proceed` button to submit the payment for review.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/after%20creating%20transaction.png)
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/after%20proceed.png)    
8.  Now login as `checker@mk` user.

> _checker@mk_: user with the checker role.

8.  we will see the payment that we created earlier. Double click the payment and after it fully loads click on the `proceed` button to approve the transaction.
    ![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/before%20proceed%20checker.png)
    ![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/after%20proceed%20checker%20EPG.png)
    
9.  The payment will now be moved to the payment instances. We can see the status of the payment as `Replied Completed` by navigating to the `Payment Instances`.
    
10.  Now login to the EPG application. Navigate to `Outward>Credit>Manual` and we will see the recently created payment with its status.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/CORPAY%20full%20payment%20cycle/after%20proceed%20checker%20EPG.png)