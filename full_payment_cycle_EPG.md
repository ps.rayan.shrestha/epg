## Full payment cycle on EPG

**Steps:**

1.  Login to EPG as `"full@hbl"` user.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/EPG_Login.png)
    
2.  Navigate to `Outward>Credit>Manual` and click the `new item` button to create a new batch for new outward credit.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/step1_credit_manual.png)

3.  Enter the required information and click `create` button to create the outward credit batch.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/step2_create_batch.png)

4.  After the creation of batch is completed, we will scroll down and click the `New Transaction` button to create a credit transaction.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/step3_create_transaction.png)
    
5.  Enter the transaction details and `save` the transaction. After saving the transaction that we just created we will have to submit the batch for approval.

6.  Navigate back to the batch list and there we will see the newly created credit batch with the status `new authorization`.
    
7.  Double click the newly created batch and after it loads completely click the `Approve` button to approve the batch. Once the batch is approved, it will begin being posted to the core banking system.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/step4_approve_transaction.png)
 
8.  Navigate back to the back list and we will see the batch status changed from CBS posting to CBS Posted. Now we will login to the ActiveMQ admin console and see the list of all the message queues.
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/CBS_POSTING.png)
![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/CBS_POSTED.png)

9.  Navigate to `Test01_01_In` queue and copy the message details.
    
10.  Navigate to the `Test01_01_Out` queue and paste the message that we copied earlier.
    ![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/2023-04-11_02-41.png)

> **Important**: Add `1/1/1,` at the beginning of the message and add a trailing `:1:1` to the end of the `SERIES.ID` property. This is to simulate the approval from the CBS.

11.  Go back to the EPG site and refresh the page. Now we will see the status of the credit batch has been changed to `Waiting IPS reply`.

![enter image description here](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20full%20payemnt%20cycle/waiting%20ips%20reply.png)