# EPG (Electronic Payment Gateway)
EPG is a software application that ***PSP (Payment Software Provider)*** uses to process the electronic transactions / online payments securely and efficiently.  The online transactions can include may tasks credit card and debit card payments, electronic wallet transactions. 
In simple words, it processes the online transactions between the merchant's website and the bank. 

### Usage
It is used to pay for the goods and services bought online. It handles the online payments, manage refunds, and other financial transactions. 

## Benefits
It provides security by using advance encryption technologies.
It makes online transactions easier by automating the payment processes.

## Parties involved in EPG
1. Merchant
2. Customer
3. Payment Processor
4. Payment Gateway

# HBL (Himalayan Bank Limited)
Himalayan bank uses the payment gateway provided by Progressoft **PS-EPG (Prgressoft Electronic Payment Gateway)**. It acts as an Interbank Payment System Payment Gateway (EPG) for all HBL domestic payments.   

PS-EPG (Progressoft Electronic Payment Gateway)  acts as a payment gateway for the HBL to perform domestic interbank credit transfers as well as debit transactions and it also acts as an online interface channel between their systems and the centrally hosted Interbank Payment System (IPS). It allows the users at HBL to initiate payment transactions directly on the customer's accounts. It also allows user to directly perform the post financial transactions directly on the customer's account. It also provides an interface for the the **Himal Remit (HR) system**. It enables to process remittance in any other banks. It provides all the needed Inward and Outward payments processing interfacing options with HBL core banking system.

### Three main components: 
1. **Core Banking System Online Integration**: It handles the payment initiations from the users and pass that information to the core banking system. 

2. **NCHL-IPS Integration and Reconciliation**: It handles the payment reconciliation by communicating the outward and inward payment to/from the NCHL-IPS system.

3. **Payment Gateway**: It integrates the internal payment system with the NCHL-IPS. 

> Microsoft SQL Server enterprise edition us used by the EPG system as the database engine.

**It checks the batch and transaction informations and generates a transaction messages in the ISO20022 standard which includes informations related to validating the transaction and approval messages, and passing the transaction details between the maker and checker users.**
