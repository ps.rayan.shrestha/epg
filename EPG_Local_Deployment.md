## Changes made 
### Active MQ 
1. Navigate to conf folder in the ActiveMQ's installation location. 
2. Inside the conf sub-directory we have to make some changes in two files (activemq.xml, jetty.xml).
##### [activemq.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/ActiveMQ%20xml%20file%20comment%20out.png):
Comment out the transportConnectors except the operwire and change the maximumConnections to 10000 from 1000.
```
<transportConnectors>
<!-- DOS protection, limit concurrent connections to 1000 and frame size to 100MB -->
<transportConnector name="openwire" uri="tcp://0.0.0.0:61616?maximumConnections=10000&amp;wireFormat.maxFrameSize=104857600"/>
<!--<transportConnector name="amqp" uri="amqp://0.0.0.0:5672?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="stomp" uri="stomp://0.0.0.0:61613?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="mqtt" uri="mqtt://0.0.0.0:1883?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
<transportConnector name="ws" uri="ws://0.0.0.0:61614?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/> -->
</transportConnectors> 
```
##### [jetty.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/jetty%20xml%20file.png):
Change the port number if wanted. The default port number is `8161`. 

### Apache Tomcat
1. Navigate to the server [`server.xml`](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/Tomcat%20port%20change.png) and change the default port **88080** to any other port. 

```
<Connector port="1212" protocol="HTTP/1.1"
	connectionTimeout="20000"
	redirectPort="8443" />
 ```

2. Navigate to the [`tomcat-users.xml`](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/Tomcat%20User%20config.png) file and add users and their roles. This is required in order to access the host-manager, and webapps page. 

```
<role rolename="admin-gui"/>
<role rolename="manager-gui"/>
<user username="rayan" password="rayan" roles="admin-gui, manager-gui"/>
```

3. Move the contents of the WAR file of the hbl to the webapp directory inside the tomcat installation directory. 

4. Check the configurations in three different files of the hbl webapp: 
	* [webapps\hbl\WEB-INF\classes\config\core\camel\push-action-camel-config.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/hbl%20push%20action%20camel%20config.png)
	* [webapps\hbl\WEB-INF\classes\config\core\spring\applicationContext-integration.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/application%20context%20integration.png)
	* [webapps\hbl\WEB-INF\classes\config\app\camel\app-camel-config.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/app%20camel%20config.png)

5. Connect the database engine using [SQL Server Management Studio](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/connection.png).
6. [Restore the database](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/Database%20Restore%20Success.png).
7. [Create a SQL Login](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/creating%20login%20HBL.png)
 Execute the following query to create a login which will be used by EPG: 
	```
	CREATE LOGIN HBL
	WITH PASSWORD = 'HBL',
	CHECK_POLICY = OFF,
	CHECK_EXPIRATION = OFF,
	DEFAULT_DATABASE = EPGLIVE ;
8. Execute the following query to [create user, and assign roles to it, and also set it to Multi_User](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/Create%20user%20and%20set%20Multiuser.png):
	 ```
	EXEC sp_addsrvrolemember @loginame = N'HBL', @rolename = N'sysadmin';
	USE EPGLIVE
	CREATE USER HBL FROM LOGIN HBL
	EXEC sp_addrolemember
	N'db_owner', 'HBL';
	ALTER DATABASE EPGLIVE SET MULTI_USER;
9. Execute the following query to [change the EPG Login pas](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/update%20password.png)s for HBL EPG user `full@hbl` to "123":

	```
	update jfw_users set password='3d077da02dbcec2ccb8d8b0b82453a67' where username='full@HBL';
10. [Set the environment variable for the EPG](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/setting%20the%20enviroment%20variable%20for%20database.png):
	``` 
	setx /m db_driver "net.sourceforge.jtds.jdbc.Driver"
	setx /m db_url "jdbc:jtds:sqlserver://localhost:1433/EPGLIVE"
	setx /m db_user "HBL"
	setx /m db_password "HBL"
	setx /m db_schema "db_owner"
	setx /m db_type "MSSQL2008"
	setx /m db_validation_query "SELECT 1"
	setx /m db_check_query "SELECT COUNT FROM INFORMATION_SCHEMA.TABLES"
	setx /m db_dialect "org.hibernate.dialect.SQLServer2008Dialect"
	setx /m java_opts "-server -Xms1024m -Xmx6144m"
11. Now the configuration is complete. 
12. Start the [ActiveMQ](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/ActiveMQSuccess.png) first and then Tomcat using the startup.bat files located inside the bin subdirectory of their installation directory. 
13. Now we can access EPG at [`localhost:1212/hbl`](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/login.png) and use the following credentials to login: 
	```
	username: full@hbl
	password: 123
	tenant: HBL
### EPG HomePage
![Login Successfull](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/EPG%20Screenshots/EPG%20Homepage.png)
