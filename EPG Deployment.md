## EPG Deployment

1. Installed Virtual box
2. Installed Microsoft Windows Server 2008 R2 Standard edition fulfilling the following requirements:
>* **OS: Windows server 2008 R2 Standard Service Pack 1** 
>* **Virtual Box: 8 GB RAM, 120 GB HDD, 2 Core CPU**

3. Setup Database:
>* **MS SQL Server 2012 Enterprise Edition and SQL Server Management Studio.**
>* **SQL Server Configuration Manager to configure the network configuration for the database engine.**
>* **Allow the TCP/IP protocol on port 1433.**
>* **Restart the SQL Server.**
>* **SQL ServerManagement Studio to connect to the database engine.**

4. Installed Application server:
> * **Application Server: Apache Tomcat 8.5.57**

5. To use Apache Tomcat we need java so installed java on the Windows Server 2008.
> * **Java Version: openjdk version 1.8.0-202**
> * **I faced problem with the openjdk version so installed from oracle.**

6. Apache ActiveMQ version 5.13.0. Default port for the ActiveMQ is 8161. default admin user is "admin" with the password "admin".
