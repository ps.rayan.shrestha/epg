## Connecting and configuring ActiveMQ Application and MSSql Database

1. We will use `JDBC` to connect `ActiveMQ` with `MSSQL` and use it as the `ActiveMQ db`.

2. Download the `JDBC` driver for `MSSQL` and copy the jar file to the `ActiveMQ/lib/optional` directory.

3. Configure the `JDBC` parameters in the `activemq.xml` file. I will use the newly created `activeMQDB` database and connect to it using the `sa` user.
	```
	<bean id="mssql-ds" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
		 <property name="driverClassName" value="com.microsoft.sqlserver.jdbc.SQLServerDriver" />
		 <property name="url" value="jdbc:sqlserver://localhost:1433;DatabaseName=activeMQDB" />
		 <property name="username" value="sa" />
		 <property name="password" value="Rayan" />
		 <property name="poolPreparedStatements" value="true" />
	</bean>

![JDBC Properties](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/Database%20connection/activemq_jdbc_parameters.png)
![Database Creation](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/Database%20connection/database%20creationg.png)

4. Set up `persistence` for `ActiveMQ`.
	```
	   <persistenceAdapter>
            <jdbcPersistenceAdapter dataDirectory="${activemq.data}" dataSource="#mssql-ds" lockKeepAlivePeriod="0">
                <adapter>
                    <transact-jdbc-adapter />
                </adapter>
                <locker>
                    <lease-database-locker lockAcquireSleepInterval="10000" dataSource="#mssql-ds">
                        <statements>
                            <statements lockTableName="activemq_lock" />
                        </statements>
                    </lease-database-locker>
                </locker>
            </jdbcPersistenceAdapter>
        </persistenceAdapter>

![JDBC Persistence](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/Database%20connection/datastorage_persistant.png)

5. If the connection is successful then the `ActiveMQ` will create 3 tables in the database.
![Database connection successful](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/Database%20connection/activemq%20connected%20with%20mssql%20instance.png)