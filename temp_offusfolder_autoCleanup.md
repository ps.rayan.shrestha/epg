## Automatic deletion of temporary files inside tomcat and HBL EPG offUsFolder

1. To automate the deletion task we will use Windows Task Scheduler. 
2. Create a file and add the following commands to the file: 
	```
	@echo all
	del /q /f C:\HBL_SITE\offUsFolder\* 
	del /q /f C:\EPG\EPG Tomcat\temp\* 
	
3. Save the file with the .bat extension (Here I have save as [`TEMP_and_offUsFolder_cleanup.bat`](https://gitlab.com/ps.rayan.shrestha/epg/-/blob/main/TEMP_and_offUsFolder_cleanup.bat)).

4. Open the Task Scheduler and click on `Create Task` in the `Actions` pane on the right-hand side of the window and enter a name for the task and select the operating system you're using.
![Create Task](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/temp_cleanup_screenshots/create-task.png)

5. Select the `Triggers` tab, then click `New` to create a new trigger for the task and select the frequency and timing options for the trigger `(e.g. "Daily", "Every day at 12:00 PM")`.
![Create Triggers](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/temp_cleanup_screenshots/create-triggers.png)

6. Select the `Actions` tab, then click `New` to create a new action for the task. Choose `Start a program` as the action type and enter the full path to the batch script you created in step 4 `(e.g. C:\TEMP_and_offUsFolder_cleanup.bat)`.
![Create Action](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/temp_cleanup_screenshots/create-action.png)

7. Select the `Settings` and change the settings as the following photo:
![task restart time](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/temp_cleanup_screenshots/task-restart-time.png)

8.  Click `OK` and then right click on the task and press run to schedule the task according to the settings we specified above.