## Apache Tomcat Application logs Deletion Automation from log4j2.xml file

> *We can delete the logs by using RollingFileAppender along with the TimeBasedTriggeringPolicy and Delete action.*

1. Navigate to `log4j2.xml` file which resides inside the `WEB-INF/classes/` directory of the deployed webapp and make the following configuration change in the `RollingFile name="FILE"`:
> It can be done by two ways. 
---
>This is the first way.
```
<RollingFile name="FILE"
	fileName="${LOGGING_PATH}/FILE/FILE.${date:yyyy-MM-dd}.log"
	filePattern="${LOGGING_PATH}/FILE/ARCHEIVE/FILE.%d{yyyy-MM-dd}-%i.log.zip"
	ignoreExceptions="false">
	<DisableDataBaseTableFilter onMatch="DENY" />
	<PatternLayout Pattern="${PATTERN}">
	</PatternLayout>
	<Policies>
		<SizeBasedTriggeringPolicy size="200 KB" />
		<TimeBasedTriggeringPolicy />
	</Policies>
	<DefaultRolloverStrategy >
		<Delete basePath="${LOGGING_PATH}/FILE/ARCHEIVE" maxDepth="1">
			<IfFileName glob="FILE*.zip" />
			<IfLastModified age="1d" />
		</Delete>
	</DefaultRolloverStrategy>
</RollingFile>
```
>This is the another way.
```
<RollingFile name="FILE" 
	fileName="${LOGGING_PATH}/FILE/FILE.${date:yyyy-MM-dd}.log"
    filePattern="${LOGGING_PATH}/FILE/ARCHEIVE/FILE.%d{yyyy-MM-dd}-%i.log.zip">
    <PatternLayout Pattern="${PATTERN}">
    </PatternLayout>
    <Policies>
	    <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
	    <SizeBasedTriggeringPolicy size="200 KB"/>
    </Policies>
    <DefaultRolloverStrategy max="2"/>
</RollingFile>
```
2. Restart the  tomcat and all the logs older that 1 day will be deleted. 

**Configuration changes in the log4j2.xml file**
![Changes in log4j2.xml](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/log4j/LOG4J.png)

**Before logs deletion**
![Before](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/log4j/before%20log%20deletion.png)

**After logs deletion**
![After](https://gitlab.com/ps.rayan.shrestha/epg/-/raw/main/log4j/logs%20deleted.png)